<?php

declare(strict_types=1);

namespace Smtm\Elastic;

use Smtm\Base\Infrastructure\Psr\Log\Factory\LaminasPsrLoggerFactory;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Elastic\Infrastructure\Psr\Log\Formatter\LaminasElasticCommonSchemaFormatter;
use Smtm\Elastic\Infrastructure\Psr\Log\LaminasPsrElasticApmErrorLogger;
use Laminas\Log\Writer\AbstractWriter;
use Laminas\Log\Writer\Stream;
use Psr\Container\ContainerInterface;

return [
    'delegators' => [
        InfrastructureServicePluginManager::class  => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
                $infrastructureServicePluginManager = $callback();

                $infrastructureServicePluginManager->setFactory(
                    LaminasPsrElasticApmErrorLogger::class,
                    LaminasPsrLoggerFactory::class
                );
                $infrastructureServicePluginManager->setFactory(
                    'logger-elastic',
                    function (
                        ContainerInterface $container,
                        $name,
                        array $options = null
                    ) {
                        $infrastructureServicePluginManager = $container->get(InfrastructureServicePluginManager::class);
                        $logger = null;

                        try {
                            /** @var LaminasPsrElasticApmErrorLogger $logger */
                            $logger = $infrastructureServicePluginManager->build(LaminasPsrElasticApmErrorLogger::class);

                            /** @var AbstractWriter $writerStderr */
                            $writer = $logger->writerPlugin(
                                Stream::class,
                                ['stream' => 'data/logs/ecs/error.json']
                            );
                            $writer->setFormatter(
                                $writer->formatterPlugin(LaminasElasticCommonSchemaFormatter::class)
                            );

                            $logger->addWriter($writer);
                        } catch (\Throwable $t) {
                            echo $t;
                            // Skip throwing of exceptions to avoid userd login problems.
                        }

                        return $logger;
                    }
                );

                return $infrastructureServicePluginManager;
            },
        ],
    ],
];
