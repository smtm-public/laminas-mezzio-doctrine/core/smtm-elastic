<?php

declare(strict_types=1);

namespace Smtm\Base;

use Smtm\Elastic\Factory\ElasticConfigAwareDelegator;
use Smtm\Elastic\Infrastructure\Psr\Log\Formatter\LaminasElasticCommonSchemaFormatter;

return [
    'log_formatters' => [
        'delegators' => [
            LaminasElasticCommonSchemaFormatter::class => [
                ElasticConfigAwareDelegator::class,
            ],
        ],
    ],
];
