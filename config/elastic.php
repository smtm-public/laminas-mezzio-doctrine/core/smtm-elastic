<?php

declare(strict_types=1);

namespace Smtm\Elastic;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-elastic')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../',
        '.env.smtm.smtm-elastic'
    );
    $dotenv->load();
}

$service = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_ELASTIC_SERVICE'),
    true,
    flags: JSON_THROW_ON_ERROR
);
$serviceElasticsearch = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_ELASTIC_ELASTICSEARCH_SERVICES'),
    true,
    flags: JSON_THROW_ON_ERROR
);
$serviceKibana = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_ELASTIC_KIBANA_SERVICES'),
    true,
    flags: JSON_THROW_ON_ERROR
);

return [
    'service' => $service,
    'elasticsearch' => [
        'services' => $serviceElasticsearch,
    ],
    'kibana' => [
        'services' => $serviceKibana,
    ],
];
