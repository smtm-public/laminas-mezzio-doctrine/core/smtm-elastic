<?php

declare(strict_types=1);

namespace Smtm\Elastic;

use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigProvider
{
    #[ArrayShape(['dependencies' => 'array', 'elastic' => 'array'])] public function __invoke(): array
    {
        return [
            'dependencies' => include __DIR__ . '/../config/dependencies.php',
            'elastic' => include __DIR__ . '/../config/elastic.php',
        ] + include __DIR__ . '/../config/laminas/laminas-log/log.php';
    }
}
