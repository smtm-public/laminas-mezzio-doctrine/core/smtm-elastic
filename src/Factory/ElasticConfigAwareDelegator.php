<?php

declare(strict_types=1);

namespace Smtm\Elastic\Factory;

use Smtm\Base\Factory\ConfigAwareDelegator;

class ElasticConfigAwareDelegator extends ConfigAwareDelegator
{
    protected array|string|null $configKey = 'elastic';
}
