<?php

declare(strict_types=1);

namespace Smtm\Elastic\Infrastructure\Psr\Log;

use Smtm\Base\Infrastructure\Helper\ThrowableHelper;
use Smtm\Base\Infrastructure\LaminasPsrLogger;
use Elastic\Apm\ElasticApm;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LaminasPsrElasticApmErrorLogger extends LaminasPsrLogger
{
    public function error($message, $extra = [])
    {
        if (!class_exists(ElasticApm::class)) {
            return parent::error('Cannot log to the Elasticsearch error stream. The Elastic APM Agent has not been enabled.', $extra);
        }

        /** @var \Throwable $rootException */
        $rootException = $message;

        while ($rootException->getPrevious()) {
            $rootException = $rootException->getPrevious();
        }

        ElasticApm::createErrorFromThrowable($rootException);
//        $customErrorData = new CustomErrorData();
//        $customErrorData->type = get_class($message);
//        $customErrorData->message = ThrowableHelper::formatAsString($message);
//        $customErrorData->code = $message->getCode();
//        ElasticApm::createCustomError($customErrorData);

        return parent::error(ThrowableHelper::formatAsString($message), $extra);
    }

//    public function log($level, $message, $context = [])
//    {
//        ElasticApm::getCurrentTransaction()->
//
//        parent::log($level, $message, $context);
//    }
}
