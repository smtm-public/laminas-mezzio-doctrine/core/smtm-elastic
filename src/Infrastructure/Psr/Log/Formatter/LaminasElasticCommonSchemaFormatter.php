<?php

declare(strict_types=1);

namespace Smtm\Elastic\Infrastructure\Psr\Log\Formatter;

use Smtm\Base\ConfigAwareInterface;
use Smtm\Base\ConfigAwareTrait;
use Elastic\Apm\ElasticApm;
use Elastic\Apm\TransactionInterface;
use Elastic\Monolog\Formatter\ElasticCommonSchemaFormatter;
use Elastic\Types\Error as EcsError;
use Elastic\Types\Service;
use Elastic\Types\Tracing;
use Laminas\Log\Formatter\FormatterInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LaminasElasticCommonSchemaFormatter extends ElasticCommonSchemaFormatter implements
    FormatterInterface, ConfigAwareInterface
{

    use ConfigAwareTrait;

    protected string $dateTimeFormat = self::DEFAULT_DATETIME_FORMAT;

    public function format($record): string
    {
        if (!class_exists(ElasticApm::class)) {
            return 'Cannot format the log message. The Elastic APM Agent has not been enabled.';
        }

        if (isset($record['priorityName']) && is_string($record['priorityName'])) {
            $record['level_name'] = $record['priorityName'];
            unset($record['priorityName']);
            unset($record['priority']);
        }

        if (isset($record['timestamp']) && $record['timestamp'] instanceof \DateTimeInterface) {
            $record['datetime'] = $record['timestamp'];
            unset($record['timestamp']);
        }

        if (isset($record['extra']['throwable']) && $record['extra']['throwable'] instanceof \Throwable) {
            /** @var \Throwable $throwable */
            $throwable = $record['extra']['throwable'];
            $record['extra']['file'] = $throwable->getFile();
            $record['extra']['line'] = $throwable->getLine();
            $record['extra']['error'] = new EcsError($record['extra']['throwable']);
            unset($record['extra']['throwable']);
        }

        if (isset($record['extra']['loggerName']) && is_string($record['extra']['loggerName'])) {
            $record['channel'] = $record['extra']['loggerName'];
        } elseif (isset($record['extra']['logger'])) {
            if (is_string($record['extra']['logger'])) {
                $record['channel'] = $record['extra']['logger'];
            } elseif (is_object($record['extra']['logger'])) {
                $record['channel'] = get_class($record['extra']['logger']);
            }
        } else {
            $record['channel'] = 'unknown logger';
        }

        unset($record['extra']['loggerName']);
        unset($record['extra']['logger']);

        $service = new Service();
        $service->setName($this->config['service']['name']);
        $service->setVersion($this->config['service']['version']);
        $record['context']['service']['Elastic\Types\Service'] = $service;
        /** @var TransactionInterface $transaction */
        $transaction = ElasticApm::getCurrentTransaction();
        $tracing = new Tracing($transaction->getTraceId(), $transaction->getId());
        unset($record['extra']['request']);
        $record['context']['tracing']['Elastic\Types\Tracing'] = $tracing;

        return parent::format($record);
    }
    /**
     * {@inheritDoc}
     */
    public function getDateTimeFormat(): string
    {
        return $this->dateTimeFormat;
    }

    /**
     * {@inheritDoc}
     */
    public function setDateTimeFormat($dateTimeFormat)
    {
        $this->dateTimeFormat = (string) $dateTimeFormat;

        return $this;
    }
}
