<?php

declare(strict_types=1);

namespace Smtm\Elastic\Command;

use Smtm\Base\Command\Command;
use Smtm\Base\Command\CommandInitializerInterface;
use Elastic\Apm\ElasticApm;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ElasticApmCommandInitializer implements CommandInitializerInterface
{
    public static function initialize(Command $command, InputInterface $input, OutputInterface $output): void
    {
        if (!class_exists(ElasticApm::class)) {
            $output->writeln('Cannot run ' . __METHOD__ . '. The Elastic APM Agent has not been enabled.');

            return;
        }

        $transaction = ElasticApm::getCurrentTransaction();
        $transaction->setName($command->getName());
    }
}
